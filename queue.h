#ifndef ZALUPA_EBANAYA
#define ZALUPA_EBANAYA

class queue
{
	int *arr;   
	int capacity; 
	int front;  
	int rear;   	
	int count;  

public:
	queue(int size = 10000);	 

	void pop();
	void push(int x);
	int get_front();
	bool empty();
};


#endif