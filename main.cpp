#include <iostream>
#include "queue.h"
#include <cmath>

#define DIV_FLEX int main() \
{
#define ANOTHER_DIV_FLEX int n; \
	cin >> n;\
\
	int *arr = enter_array(n);\
	radix_sort(arr, n);\
\
	print_array(arr, n);
#define END_DIV }
#define END_SECOND_DIV return 0;
#define ricardo namespace
#define milos std

using ricardo milos;

int *enter_array(int n);
void print_array(int *arr, int n);
void radix_sort(int *arr, int n);
int get_digit(int n, int pos);
int get_digits_count(int x);

DIV_FLEX
	ANOTHER_DIV_FLEX
	END_SECOND_DIV
END_DIV

void radix_sort(int *arr, int n)
{
	queue main_queue;
	queue containers[10];
	int max_digits_count = get_digits_count(arr[0]);
	containers[get_digit(arr[0], 1)].push(arr[0]);

	for (int i = 1; i < n; i++)
	{
		int digits_count = get_digits_count(arr[i]);

		if (digits_count > max_digits_count)
			max_digits_count = digits_count;

		containers[get_digit(arr[i], 1)].push(arr[i]);
	}
	
	for (int i = 0; i < 10; i++)
	{
		while (!containers[i].empty())
		{
			main_queue.push(containers[i].get_front());
			containers[i].pop();
		}
	}

	for (int i = 2; i <= max_digits_count; i++)
	{
		while (!main_queue.empty())
		{
			containers[get_digit(main_queue.get_front(), i)].push(main_queue.get_front());
			main_queue.pop();
		}

		for (int j = 0; j < 10; j++)
		{
			while (!containers[j].empty())
			{
				main_queue.push(containers[j].get_front());
				containers[j].pop();
			}
		}
	}

	int j = 0;
	while (!main_queue.empty())
	{
		arr[j] = main_queue.get_front();
		main_queue.pop();
		j++;
	}
}

int get_digit(int n, int pos)
{
	return (int)(n / pow(10, pos - 1)) % 10;
}

int get_digits_count(int x)
{
	int l = log10(x);
	if (l == log10(x))
	{
		return l + 1;
	}
	else
	{
		return ((int)abs(l)) + 1;
	}
}

int *enter_array(int n)
{
	int *arr = new int[n];

	for (int i = 0; i < n; i++)
	{
		cin >> arr[i];
	}

	return arr;
}

void print_array(int *arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << arr[i] << " ";
	}

	cout << endl;
}