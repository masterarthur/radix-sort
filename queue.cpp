#include "queue.h"

queue::queue(int size)
{
	arr = new int[size];
	capacity = size;
	front = 0;
	rear = -1;
	count = 0;
}


void queue::pop()
{
	front = (front + 1) % capacity;
	count--;
}

void queue::push(int item)
{
	rear = (rear + 1) % capacity;
	arr[rear] = item;
	count++;
}

int queue::get_front()
{
	return arr[front];
}

bool queue::empty()
{
	return count == 0;
}
